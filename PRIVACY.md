# Privacy Statement

The plugin "All branches dashboard" does not collect any user data nor does it collect any usage data.

For everything else, the official [Atlassian Privacy Policy](https://www.atlassian.com/legal/privacy-policy) applies.
