<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.github.sylvainlaurent.gitdescribestashplugin</groupId>
  <artifactId>stash-branch-dashboard</artifactId>
  <version>1.2.7-SNAPSHOT</version>
  <packaging>atlassian-plugin</packaging>
  <name>stash-branch-dashboard</name>
  <description>A Atlassian Bitbucket Server plugin to show all the branches of all git repositories of a Bitbucket project. The last tag of each branch is also displayed (i.e. git describe)</description>
  <organization>
    <name>Sylvain Laurent</name>
    <url>https://github.com/sylvainlaurent/</url>
  </organization>

  <licenses>
    <license>
      <name>The Apache Software License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
    </license>
  </licenses>

  <developers>
    <developer>
      <name>Sylvain LAURENT</name>
      <email>slaurent@apache.org</email>
    </developer>
  </developers>
  <scm>
    <url>https://bitbucket.org/sylvain_laurent/stash-branch-dashboard</url>
    <developerConnection>scm:git:https://bitbucket.org/sylvain_laurent/stash-branch-dashboard.git</developerConnection>
  </scm>
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <bitbucket.version>6.4.0</bitbucket.version>
    <bitbucket.data.version>${bitbucket.version}</bitbucket.data.version>
    <amps.version>8.0.2</amps.version>
    <plugin.testrunner.version>1.2.3</plugin.testrunner.version>
    <atlassian.spring.scanner.version>2.1.7</atlassian.spring.scanner.version>
    <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
    <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    <byte-buddy.version>1.9.7</byte-buddy.version>
    <!-- for the moment there are no real integration tests, just skip them -->
    <skipITs>true</skipITs>
  </properties>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.atlassian.bitbucket.server</groupId>
        <artifactId>bitbucket-parent</artifactId>
        <version>${bitbucket.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>net.bytebuddy</groupId>
        <artifactId>byte-buddy</artifactId>
        <version>${byte-buddy.version}</version>
      </dependency>
      <dependency>
        <groupId>net.bytebuddy</groupId>
        <artifactId>byte-buddy-agent</artifactId>
        <version>${byte-buddy.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>com.atlassian.sal</groupId>
      <artifactId>sal-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.bitbucket.server</groupId>
      <artifactId>bitbucket-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.bitbucket.server</groupId>
      <artifactId>bitbucket-spi</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.bitbucket.server</groupId>
      <artifactId>bitbucket-page-objects</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>commons-lang</groupId>
      <artifactId>commons-lang</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.plugin</groupId>
      <artifactId>atlassian-spring-scanner-annotation</artifactId>
      <version>${atlassian.spring.scanner.version}</version>
      <scope>provided</scope>
    </dependency>


    <dependency>
      <groupId>javax.inject</groupId>
      <artifactId>javax.inject</artifactId>
      <version>1</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.soy</groupId>
      <artifactId>soy-template-renderer-api</artifactId>
      <scope>provided</scope>
    </dependency>

    <!-- WIRED TEST RUNNER DEPENDENCIES -->
    <dependency>
      <groupId>com.atlassian.plugins</groupId>
      <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
      <version>${plugin.testrunner.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>javax.ws.rs</groupId>
      <artifactId>jsr311-api</artifactId>
      <version>1.1.1</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <version>2.24.5</version>
      <scope>test</scope>
    </dependency>

  </dependencies>
  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>3.0.0-M3</version>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>bitbucket-maven-plugin</artifactId>
        <version>${amps.version}</version>
        <extensions>true</extensions>
        <configuration>
          <products>
            <product>
              <id>bitbucket</id>
              <instanceId>bitbucket</instanceId>
              <version>${bitbucket.version}</version>
              <dataVersion>${bitbucket.data.version}</dataVersion>
            </product>
          </products>
          <instructions>
            <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>

            <!-- Add package to export here -->
            <Export-Package>
            </Export-Package>

            <!-- Add package import here -->
            <Import-Package>
              *;resolution:=optional
            </Import-Package>

            <!-- Ensure plugin is Spring powered -->
            <Spring-Context>*</Spring-Context>
          </instructions>
          <enableQuickReload>true</enableQuickReload>

          <bundledArtifacts>
            <bundledArtifact>
              <groupId>com.atlassian.plugin</groupId>
              <artifactId>atlassian-spring-scanner-runtime</artifactId>
              <version>${atlassian.spring.scanner.version}</version>
            </bundledArtifact>
            <bundledArtifact>
              <groupId>com.atlassian.plugin</groupId>
              <artifactId>atlassian-spring-scanner-annotation</artifactId>
              <version>${atlassian.spring.scanner.version}</version>
            </bundledArtifact>
          </bundledArtifacts>
          <allowGoogleTracking>false</allowGoogleTracking>
        </configuration>
      </plugin>
      <plugin>
        <groupId>com.atlassian.plugin</groupId>
        <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
        <version>${atlassian.spring.scanner.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>atlassian-spring-scanner</goal>
            </goals>
            <phase>process-classes</phase>
          </execution>
        </executions>
        <configuration>
          <scannedDependencies>
            <dependency>
              <groupId>com.atlassian.plugin</groupId>
              <artifactId>atlassian-spring-scanner-external-jar</artifactId>
            </dependency>
          </scannedDependencies>
          <verbose>false</verbose>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
      <plugin>
        <groupId>external.atlassian.jgitflow</groupId>
        <artifactId>jgitflow-maven-plugin</artifactId>
        <version>1.0-m5.1</version>
        <configuration>
          <autoVersionSubmodules>true</autoVersionSubmodules>
          <scmCommentPrefix>[jgitflow]</scmCommentPrefix>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>2.8.2</version>
        <configuration>
          <!-- don't deploy to a maven repo, we just manually deploy to Atlassian MarketPlace -->
          <skip>true</skip>
        </configuration>
      </plugin>
    </plugins>
  </build>

</project>
