define('plugin/branchdashboard', [ 'jquery', 'bitbucket/util/navbuilder', 'bitbucket/util/server' ],
        function($, nav, server) {
            $(document).ready(
                    function() {
                        var fillBranchInfo = function(cell, describedBranch, showBranchName) {
                            if(describedBranch==null) {
                                return;
                            }
                            var div = $("<div class='branchColumn'/>");
                            cell = cell.append(div);
                            div.append($("<a class='aui-icon aui-icon-small aui-iconfont-devtools-branch'></a>").attr("href", describedBranch.branchNavLink))
                            if(showBranchName) {
                                div.append($("<a></a>").attr("href", describedBranch.branchNavLink).text(describedBranch.name));
                            }
                            if(describedBranch.lastTag) {
                                var additionalClass, description;
                                if(describedBranch.tagToBranchCommitDistance >0) {
                                    description = "Tip of branch is "+describedBranch.tagToBranchCommitDistance+" commit(s) after the tag.";
                                    additionalClass = "aui-lozenge-current";
                                } else {
                                    description = "Tag points to tip of branch.";
                                    additionalClass = "aui-lozenge-success";
                                }
                                div.append($("<a/>")
                                        .attr("class", "tooltiped aui-lozenge "+additionalClass)
                                        .attr("title", description+"\n"
                                                +describedBranch.lastTag.commitAuthorName +" - "+ describedBranch.lastTag.commitAuthoredFormattedDate +": "+ describedBranch.lastTag.commitMessage)
                                        .attr("href", describedBranch.commitHistoryNavLink)
                                        .text((describedBranch.tagToBranchCommitDistance>0 ? describedBranch.tagToBranchCommitDistance+" > ":"")
                                                +describedBranch.lastTag.tagName)
                                );
                            }
                        };

                        $('tr[repo-slug]').on(
                                'lazyshow',
                                function() {
                                    var repoRow = $(this);
                                    repoRow.find("td.masterBranchCol").addClass("lazy-hidden");
                                    repoRow.find("a.branchDashboardRepoName").attr("href", 
                                            nav.project($(this).attr('project-key')).repo($(this).attr('repo-slug')).branches().build() );
                                    var repoDescriptionEndpoint = nav.newBuilder(
                                            ["rest", "describe-repo", "1.0", $(this).attr('project-key'), $(this).attr('repo-slug')])
                                            .build();
                                    server.ajax({
                                        url: repoDescriptionEndpoint,
                                        success: function(result) {
                                            var masterBranchCol = repoRow.find("td.masterBranchCol");
                                            fillBranchInfo(masterBranchCol, result.masterBranch);
                                            masterBranchCol.removeClass("lazy-hidden").addClass("lazy-loaded");
                                            fillBranchInfo(repoRow.find("td.developBranchCol"), result.developBranch);
                                            if(result.featureBranches) {
                                                var col = repoRow.find("td.featureBranchesCol");
                                                result.featureBranches.forEach(function(branch){
                                                    fillBranchInfo(col, branch, true);
                                                });
                                            }
                                            if(result.hotFixBranches) {
                                                var col = repoRow.find("td.otherBranchesCol");
                                                result.hotFixBranches.forEach(function(branch){
                                                    fillBranchInfo(col, branch, true);
                                                });
                                            }
                                            if(result.otherBranches) {
                                                result.otherBranches.forEach(function(branch){
                                                    var col = repoRow.find("td.otherBranchesCol");
                                                    fillBranchInfo(col, branch, true);
                                                });
                                            }
                                        }
                                    });
                        }).lazyLoadXT({
                            visibleOnly : false
                        });
                    });
        });

AJS.$(function() {
    require('plugin/branchdashboard');
});