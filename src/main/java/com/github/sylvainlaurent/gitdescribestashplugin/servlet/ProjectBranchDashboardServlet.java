package com.github.sylvainlaurent.gitdescribestashplugin.servlet;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;

public class ProjectBranchDashboardServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private SoyTemplateRenderer soyTemplateRenderer;
    private ProjectService projectService;
    private RepositoryService repositoryService;

    @Autowired
    public ProjectBranchDashboardServlet(@ComponentImport SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport ProjectService projectService, @ComponentImport RepositoryService repositoryService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.projectService = projectService;
        this.repositoryService = repositoryService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Project project = getProject(req, resp);
        if (project == null) {
            return;
        }

        PageRequestImpl fullPage = new PageRequestImpl(0, PageRequest.MAX_PAGE_LIMIT);
        Page<? extends Repository> repositoriesPage = repositoryService
                .findByProjectKey(requireNonNull(project.getKey()), fullPage);

        ImmutableMap<String, Object> model = ImmutableMap.<String, Object> of(//
                "project", project, //
                "repositoriesPage", repositoriesPage);

        render(resp, "plugin.branchdashboard.project", model);

    }

    @Nullable
    private Project getProject(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // Get projectKey from path
        String pathInfo = req.getPathInfo();

        String[] components = pathInfo.split("/");

        if (components.length < 2) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }

        String projectKey = components[1];
        if (projectKey == null || projectKey.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }

        Project project = projectService.getByKey(projectKey);

        if (project == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        return project;
    }

    protected void render(HttpServletResponse resp, String templateName, Map<String, Object> data)
            throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.github.sylvainlaurent.gitdescribestashplugin.stash-branch-dashboard:soy-server", templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

}
