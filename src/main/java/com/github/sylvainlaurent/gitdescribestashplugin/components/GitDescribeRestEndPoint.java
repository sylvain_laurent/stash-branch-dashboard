package com.github.sylvainlaurent.gitdescribestashplugin.components;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedBranch;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Collections;
import java.util.List;

@Produces({MediaType.APPLICATION_JSON})
@Path("")
public class GitDescribeRestEndPoint {
    private static final Logger log = LoggerFactory.getLogger(GitDescribeRestEndPoint.class);

    @Autowired
    @ComponentImport
    private RepositoryService repositoryService;

    @Autowired
    private GitDescribeService gitDescribeService;

    @GET
    @Path("/{projectKey}/{slug}")
    public Response describeRepository(@PathParam("projectKey") String projectKey, @PathParam("slug") String slug) {
        try {
            Repository repo = repositoryService.getBySlug(projectKey, slug);
            if (repo == null) {
                return Response.status(Status.NOT_FOUND).build();
            }
            List<DescribedBranch> describedBranches = gitDescribeService.describeBranches(repo);
            DescribedRepository describedRepository = new DescribedRepository(repo, null, describedBranches);

            CacheControl cacheControl = new CacheControl();
            cacheControl.setNoCache(true);
            cacheControl.setNoStore(true);
            cacheControl.setPrivate(true);
            return Response.ok(describedRepository).cacheControl(cacheControl).build();
        } catch (AuthorisationException e) {
            String msg = "You don't have access to /" + projectKey + "/" + slug + " repository";
            log.info(msg);
            return Response.status(Status.FORBIDDEN).entity(Collections.singletonMap("message", msg)).build();
        }
    }
}
