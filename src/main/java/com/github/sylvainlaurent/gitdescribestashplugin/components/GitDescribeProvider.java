package com.github.sylvainlaurent.gitdescribestashplugin.components;

import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.RefMetadataContext;
import com.atlassian.bitbucket.repository.RefMetadataProvider;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedBranch;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
public class GitDescribeProvider implements RefMetadataProvider<DescribedBranch> {

    private GitDescribeService gitDescribeService;

    @Autowired
    public GitDescribeProvider(GitDescribeService gitDescribeService) {
        this.gitDescribeService = gitDescribeService;
    }

    @Nonnull
    @Override
    public Map<Ref, DescribedBranch> getMetadata(RefMetadataContext context) {
        Map<Ref, DescribedBranch> branchMap = new HashMap<>(context.getRefs().size());

        List<Branch> branchesRefs = new ArrayList<>(context.getRefs().size());
        for (Ref ref : context.getRefs()) {
            if (ref instanceof Branch) {
                branchesRefs.add((Branch) ref);
            }
        }

        List<DescribedBranch> describeBranches = gitDescribeService.describeBranches(context.getRepository(),
                branchesRefs);
        for (DescribedBranch describedBranch : describeBranches) {
            branchMap.put(describedBranch.getBranch(), describedBranch);
        }
        return branchMap;
    }

}
