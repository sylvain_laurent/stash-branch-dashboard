package com.github.sylvainlaurent.gitdescribestashplugin.components;

import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.Repository;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedBranch;

public interface GitDescribeService {

    List<DescribedBranch> describeBranches(@Nonnull Repository repository);

    List<DescribedBranch> describeBranches(@Nonnull Repository repository, List<Branch> branches);
}
