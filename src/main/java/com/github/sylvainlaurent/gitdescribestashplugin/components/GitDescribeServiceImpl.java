package com.github.sylvainlaurent.gitdescribestashplugin.components;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryBranchesRequest;
import com.atlassian.bitbucket.scm.Command;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.ScmCommandBuilder;
import com.atlassian.bitbucket.scm.ScmService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedBranch;
import com.github.sylvainlaurent.gitdescribestashplugin.model.DescribedTag;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Named
public class GitDescribeServiceImpl implements GitDescribeService {

    private RefService refService;

    private ScmService scmService;

    private final Pattern gitDescribeRegex = Pattern.compile("(.+)-(\\d+)-g([a-f0-9]{40})$");

    private NavBuilder navBuilder;

    private CommitService commitService;

    @Autowired
    public GitDescribeServiceImpl(@ComponentImport RefService refService, @ComponentImport ScmService scmService,
            @ComponentImport NavBuilder navBuilder, @ComponentImport CommitService commitService) {
        super();
        this.refService = refService;
        this.scmService = scmService;
        this.navBuilder = navBuilder;
        this.commitService = commitService;
    }

    @Override
    public List<DescribedBranch> describeBranches(@Nonnull final Repository repository) {
        PageRequestImpl fullPage = new PageRequestImpl(0, PageRequest.MAX_PAGE_LIMIT);

        RepositoryBranchesRequest repositoryBranchesRequest = new RepositoryBranchesRequest.Builder(repository).build();
        Page<Branch> branchesPage = refService.getBranches(repositoryBranchesRequest, fullPage);
        final List<Branch> branches = Lists.newArrayList(branchesPage.getValues());

        return describeBranches(repository, branches);
    }

    @Override
    public List<DescribedBranch> describeBranches(final @Nonnull Repository repository, final List<Branch> branches) {
        if (branches.isEmpty()) {
            return Collections.emptyList();
        }

        ScmCommandBuilder<?> scmCommandBuilder = scmService.createBuilder(repository);
        CommandOutputHandler<List<DescribedBranch>> commandOutputHandler = new GitDescribeOutputHandler(repository,
                branches);

        ScmCommandBuilder<?> commandBuilder = scmCommandBuilder.command("describe")//
                .argument("--tags")//
                .argument("--abbrev=40")//
                .argument("--long")//
                .argument("--always");
        for (Branch branch : branches) {
            commandBuilder = commandBuilder.argument(branch.getId());
        }
        Command<List<DescribedBranch>> command = commandBuilder.build(commandOutputHandler);

        return command.call();
    }

    private final class GitDescribeOutputHandler implements CommandOutputHandler<List<DescribedBranch>> {
        @Nonnull
        private final Repository repository;
        @Nonnull
        private final List<Branch> branches;
        private List<DescribedBranch> describedBranches = new ArrayList<>();

        private GitDescribeOutputHandler(@Nonnull Repository repository, @Nonnull List<Branch> branches) {
            this.repository = repository;
            this.branches = branches;
        }

        @Override
        public void process(InputStream output) throws ProcessException {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(output))) {
                int i;
                String line;
                for (line = reader.readLine(), i = 0; line != null; line = reader.readLine(), i++) {
                    DescribedBranch describedBranch = new DescribedBranch();

                    @SuppressWarnings("null")
                    @Nonnull
                    Branch branch = branches.get(i);
                    describedBranch.setBranch(branch);
                    describedBranch.setName(branch.getDisplayId());
                    describedBranch.setBranchNavLink(getRefNavLink(branch.getId()));
                    Matcher matcher = gitDescribeRegex.matcher(line);
                    if (matcher.matches()) {
                        @SuppressWarnings("null")
                        @Nonnull
                        String tagName = matcher.group(1);

                        int tagToBranchCommitDistance = Integer.parseInt(matcher.group(2));

                        Commit commit = commitService.getCommit(new CommitRequest.Builder(repository, tagName).build());
                        DescribedTag describedTag = new DescribedTag(tagName, getRefNavLink(tagName), commit);

                        describedBranch.setLastTag(describedTag, tagToBranchCommitDistance,
                                getCommitHistoryNavLink(branch.getId()));
                    }
                    describedBranches.add(describedBranch);
                }
            } catch (IOException e) {
                throw new ProcessException(e);
            }
        }

        @Override
        public void complete() {
            // System.out.println("command complete");
        }

        @Override
        public void setWatchdog(Watchdog watchdog) {
        }

        @Override
        public List<DescribedBranch> getOutput() {
            return describedBranches;
        }

        private String getCommitHistoryNavLink(@Nonnull String ref) {
            return navBuilder.repo(repository).commits().until(ref).buildRelative();
        }

        private String getRefNavLink(@Nonnull String ref) {
            return navBuilder.repo(repository).browse().atRevision(ref).buildRelative();
        }
    }
}
