package com.github.sylvainlaurent.gitdescribestashplugin.model;

import com.atlassian.bitbucket.repository.Repository;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@JsonAutoDetect
public class DescribedRepository {
    @JsonIgnore
    private final Repository repository;
    private final String navLink;

    private final List<DescribedBranch> featureBranches = new ArrayList<>();
    private final List<DescribedBranch> hotFixBranches = new ArrayList<>();
    @Nullable
    private final DescribedBranch developBranch;

    @Nullable
    private final DescribedBranch masterBranch;
    private final List<DescribedBranch> otherBranches = new ArrayList<>();

    public DescribedRepository(Repository repository, String navLink, List<DescribedBranch> branches) {
        this.repository = repository;
        this.navLink = navLink;

        DescribedBranch developBranchLocal = null;
        DescribedBranch masterBranchLocal = null;

        for (DescribedBranch branch : branches) {
            String branchName = branch.getName();
            if (branchName.startsWith("feature/")) {
                featureBranches.add(branch);
            } else if (branchName.startsWith("hotfix/")) {
                hotFixBranches.add(branch);
            } else if (branchName.equals("develop")) {
                developBranchLocal = branch;
            } else if (branchName.equals("master")) {
                masterBranchLocal = branch;
            } else {
                otherBranches.add(branch);
            }
        }
        developBranch = developBranchLocal;
        masterBranch = masterBranchLocal;

        featureBranches.sort(BranchNameComparator.INSTANCE);
        hotFixBranches.sort(BranchNameComparator.INSTANCE);
        otherBranches.sort(BranchNameComparator.INSTANCE);
    }

    public Repository getRepository() {
        return repository;
    }

    public String getNavLink() {
        return navLink;
    }

    public List<DescribedBranch> getFeatureBranches() {
        return featureBranches;
    }

    public List<DescribedBranch> getHotFixBranches() {
        return hotFixBranches;
    }

    @Nullable
    public DescribedBranch getDevelopBranch() {
        return developBranch;
    }

    @Nullable
    public DescribedBranch getMasterBranch() {
        return masterBranch;
    }

    public List<DescribedBranch> getOtherBranches() {
        return otherBranches;
    }

    private static class BranchNameComparator implements Comparator<DescribedBranch> {

        static BranchNameComparator INSTANCE = new BranchNameComparator();

        @Override
        public int compare(DescribedBranch o1, DescribedBranch o2) {
            return o1.getName().compareTo(o2.getName());
        }

    }
}
