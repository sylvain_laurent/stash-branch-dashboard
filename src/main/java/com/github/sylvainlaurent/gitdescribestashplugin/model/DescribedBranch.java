package com.github.sylvainlaurent.gitdescribestashplugin.model;

import com.atlassian.bitbucket.repository.Branch;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.annotation.Nullable;

@JsonAutoDetect
public class DescribedBranch {
    @JsonIgnore
    private Branch branch;

    private String name;

    @Nullable
    private DescribedTag lastTag;
    @Nullable
    private Integer tagToBranchCommitDistance;
    private String branchNavLink;
    @Nullable
    private String commitHistoryNavLink;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DescribedTag getLastTag() {
        return lastTag;
    }

    public void setLastTag(DescribedTag lastTag) {
        this.lastTag = lastTag;
    }

    public Integer getTagToBranchCommitDistance() {
        return tagToBranchCommitDistance;
    }

    public void setTagToBranchCommitDistance(Integer tagToBranchCommitDistance) {
        this.tagToBranchCommitDistance = tagToBranchCommitDistance;
    }

    public String getBranchNavLink() {
        return branchNavLink;
    }

    public void setBranchNavLink(String navLink) {
        this.branchNavLink = navLink;
    }

    @Nullable
    public String getCommitHistoryNavLink() {
        return commitHistoryNavLink;
    }

    public void setCommitHistoryNavLink(String commitHistoryNavLink) {
        this.commitHistoryNavLink = commitHistoryNavLink;
    }

    @Override
    public String toString() {
        return "DescribedBranch [branch=" + name + ", lastTag=" + lastTag + ", tagToBranchCommitDistance="
                + tagToBranchCommitDistance + "]";
    }

    public void setLastTag(DescribedTag describedTag, int tagToBranchCommitDistance, String commitHistoryNavLink) {
        setLastTag(describedTag);
        setTagToBranchCommitDistance(tagToBranchCommitDistance);
        setCommitHistoryNavLink(commitHistoryNavLink);
    }

}
