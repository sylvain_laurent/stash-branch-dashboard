package com.github.sylvainlaurent.gitdescribestashplugin.model;

import com.atlassian.bitbucket.commit.Commit;
import org.apache.commons.lang3.time.FastDateFormat;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class DescribedTag {
    private final static FastDateFormat DATE_TIME = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ssX");
    private final String tagName;
    private final String navLink;
    private String commitAuthorName;
    private String commitAuthoredFormattedDate;
    private String commitMessage;

    public DescribedTag(String tagName, String navLink, Commit commit) {
        this.tagName = tagName;
        this.navLink = navLink;
        this.commitAuthorName = commit.getAuthor().getName();
        this.commitMessage = commit.getMessage();
        this.commitAuthoredFormattedDate = DATE_TIME.format(commit.getAuthorTimestamp());
    }

    public String getTagName() {
        return tagName;
    }

    public String getNavLink() {
        return navLink;
    }

    public String getCommitAuthorName() {
        return commitAuthorName;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    public String getCommitAuthoredFormattedDate() {
        return commitAuthoredFormattedDate;
    }

    public void setCommitAuthoredFormattedDate(String commitAuthoredFormattedDate) {
        this.commitAuthoredFormattedDate = commitAuthoredFormattedDate;
    }

}
