package ut.com.github.sylvainlaurent.gitdescribestashplugin.components;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class GitDescribeServiceImplTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected = Exception.class)
    public void testSomething() throws Exception {

        // GitDescribeServiceImpl testClass = new GitDescribeServiceImpl();

        throw new Exception("GitDescribeServiceImpl has no tests!");

    }

}
